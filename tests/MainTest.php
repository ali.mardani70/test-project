<?php

namespace TDD\Test;
require dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';

use PHPUnit\Framework\TestCase;
use TDD\main;

class MainTest extends TestCase
{

    public function testLoadData()
    {
        $main = new main();
        $this->assertTrue($main->loadData("test.txt"), "loading Data successful");
    }

    public function testGetRate()
    {
        $main = new main();
        $main->loadData("test.txt");
        $this->assertNotEmpty($main->getRates(), "rate must not be empty!");
    }

    /**
     * @dataProvider getDataProvider
     */
    public function testGetData($data, $expected, $id = null)
    {
        $main = new main();
        $main->data = $data;
        $this->assertEquals($expected, $main->getData($id), "item must be " . print_r($expected));
    }

    public function getDataProvider()
    {
        $data = [
            "0" => ["bin" => "45717360", "amount" => "100.00", "currency" => "EUR"],
            "1" => ["bin" => "516793", "amount" => "50.00", "currency" => "USD"]
        ];
        return [
            [$data, $data["0"]],
            [$data, $data["1"], 45717360]
        ];
    }

    /**
     * @dataProvider roundUpProvider
     */
    public function testRoundUp($value, $precision ,$expected  )
    {
        $main = new main();
        $this->assertEquals($expected, $main->RoundUp($value, $precision), "item must be " . $expected );
    }

    public function roundUpProvider()
    {
        return [
           [ 111.111 , 2 , 111.12] ,
           [ 41.4567 , 2 , 41.46] ,
           [ 41.4 , 2 , 41.4]
        ];
    }

    /**
     * @dataProvider isEuProvider
     */
    public function testIsEu( $value , $expected){
        $main = new main();
        $this->assertEquals($expected, $main->isEu( $value ), " item must be " . (string)$expected );
    }
    public function isEuProvider(){
        return [
            ['CZ' , true ],
            ['JP' , false ]
        ];
    }

}