<?php

require dirname(__FILE__).DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.'autoload.php';

$mainObj = new \TDD\main();
$mainObj->loadData($argv[1]);
$item = $mainObj->getData();

while( !empty($item) && $item ){
    $binResults = file_get_contents('https://lookup.binlist.net/' .$item['bin']);
    if (!$binResults)
        die('error!');
    $r = json_decode($binResults);
    $isEu =  $mainObj->isEu($r->country->alpha2);
    $rate = $mainObj->getRates($item['currency']);
    $amntFixed = $mainObj->getAmntFixed( $item['currency'] , $rate , $item['amount'] );
    echo  $mainObj->RoundUp( $amntFixed * ( $isEu ? 0.01 : 0.02) , 2);
    print "\n";
    $item = $mainObj->getData( $item["bin"] );
}
