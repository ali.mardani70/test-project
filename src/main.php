<?php

namespace TDD;

class main
{
    public $data, $rates , $countries;
    function __construct() {
        $this->countries = ['AT', 'BE', 'BG', 'CY', 'CZ', 'DE', 'DK', 'EE', 'ES', 'FI', 'FR', 'GR', 'HR', 'HU', 'IE', 'IT', 'LT', 'LU', 'LV', 'MT', 'NL', 'PO', 'PT', 'RO', 'SE', 'SI', 'SK'];
    }
    public function loadData($path)
    {
        $tempData = explode("\n", file_get_contents($path));
        foreach ($tempData as $row) {
            $rowData = json_decode($row, true);
            $this->data[] = $rowData;
        }
        $this->rates = @json_decode(file_get_contents('http://api.exchangeratesapi.io/latest?access_key=05b4d79767cd72b70e1458ca05566ae8'), true)['rates'];
        if (empty($this->data) || empty($this->rates)) return false;
        return true;
    }

    public function getRates($key = null)
    {
        if (empty($key))
            return $this->rates;
        return $this->rates[$key];
    }

    public function getData($id = null)
    {
        $nextElement = false;
        if (empty($id)){
            $nextElement = current($this->data);
        }else{
            foreach ( $this->data as $index => $item) {
                if ( $item["bin"] == $id && !empty($this->data[$index+1]) ) {
                    $nextElement = $this->data[$index+1];
                }
            }
        }
        return $nextElement;
    }

    public function isEu($c)
    {
        $result = false;
        return in_array($c, $this->countries );
    }

    public function RoundUp($value, int $precision)
    {
        $pow = pow(10, $precision);
        return (ceil($pow * $value) + ceil($pow * $value - ceil($pow * $value))) / $pow;
    }

    public function getAmntFixed($currency, $rate , $amount)
    {
        if ( $currency == 'EUR' || $rate == 0 ) {
            $amntFixed = $amount;
        } elseif ( $currency  != 'EUR' || $rate > 0) {
            $amntFixed = $amount / $rate;
        }
        return $amntFixed;
    }

}